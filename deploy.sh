#!/bin/sh

# AUTOMATIC DEPLOY SCRIPT
# to deploy a file to $HOME place it in the homedir/ folder
# to deploy to the .config folder place the configuration folder in the config/ folder

CONFIG_FOLDER=".config"

echo "[INFO] Checking /home/`whoami`/ links..."

HOMEFILES=`find homedir/ -type f -o -type d ! -path homedir/ -maxdepth 1`
for SRCPATH in ${HOMEFILES}; do
    TARGETPATH="${HOME}/`basename ${SRCPATH}`"
    [ -L ${TARGETPATH} ] || (echo -n "[LINK] " && ln -vs "${PWD}/${SRCPATH}" "${TARGETPATH}")
done

if [ ${XDG_CONFIG_DIR} ]; then
    CONFIG_TARGET="${XDG_CONFIG_DIR}"
else
    CONFIG_TARGET="${HOME}/.config/"
fi

echo "[INFO] Checking ${CONFIG_TARGET} links..."

CONFIGFILES=`find config/ -type f -o -type d ! -path config/ -maxdepth 1`
for SRCPATH in ${CONFIGFILES}; do
    TARGETPATH="${CONFIG_TARGET}/`basename ${SRCPATH}`"
    [ -L ${TARGETPATH} ] || (echo -n "[LINK] " && ln -vs "${PWD}/${SRCPATH}" "${TARGETPATH}")
done

if [ -d "${HOME}/.local/bin" ]; then
    echo "[INFO] ~/.local/bin exists"
else
    echo "[INFO] Creating ~/.local/bin/..."
    mkdir -p ${HOME}/.local/bin
fi

if [ -d "${HOME}/.local/share" ]; then
    echo "[INFO] ~/.local/share exists"
else
    echo "[INFO] Creating ~/.local/share/..."
    mkdir -p ${HOME}/.local/share
fi

echo "[INFO] Calling Stow to install symlinks"
stow -S -t ${HOME}/.local -d. .local
