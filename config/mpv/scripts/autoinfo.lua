-- Copyright 2022 Nico Sonack <nsonack@herrhotzenplotz.de>
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions
-- are met:
--
-- 1. Redistributions of source code must retain the above copyright
--    notice, this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above
--    copyright notice, this list of conditions and the following
--    disclaimer in the documentation and/or other materials provided
--    with the distribution.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
-- "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
-- LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
-- FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
-- COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
-- INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
-- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
-- SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
-- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
-- STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
-- OF THE POSSIBILITY OF SUCH DAMAGE.

--
-- autoinfo.lua: Dump file metadata into a file everytime we start
--               playing a new file. This metadata file is read by
--               i3status and displayed in the i3 status bar.
--
--    To autoload this script, put it into
--    ${HOME}/.config/mpv/scripts/.
--

local mp = require 'mp'
local options = require 'mp.options'
local utils = require 'mp.utils'
local io = require 'io'
local iconv = require 'iconv'
local os = require 'os'

local converter = iconv.new("utf-8", "iso-8859-1")

-- Hook function
local function on_new_metadata()
   -- Request file metadata
   local meta = mp.get_property_native("metadata")

   -- Wtf?
   if meta == nil then
      return
   end

   -- Result variables
   local Artist = "No artist"
   local Album = "No album"
   local Title = "No Title"

   local Result = nil

   -- Extract info
   if meta.artist ~= nil then Artist = meta.artist end
   if meta.album ~= nil then Album = meta.album end
   if meta.title ~= nil then Title = meta.title end

   -- Check if we got anything at all
   if meta.artist == nil and meta.album == nil and meta.title == nil then
      -- Is this icecast?
      local icytitle = meta["icy-title"]

      if icytitle == nil then
         -- Nope!
         Result = "No track info available"
      else
         -- Yay!
         Result = icytitle
      end

   else
      -- Dump the data
      Result = Artist .. " - " .. Album .. " - " .. Title
   end

   -- Try Iconv to prevent invalid encoding in JSON
   local Text, e = converter:iconv(Result)

   -- Open our dump file
   local ofile = io.open(os.getenv("HOME") .. "/.music-data","w")

   -- Dump
   if e == nil then
      ofile:write(Text)
   else
      ofile:write(Result)
   end

   -- Close (and flush) the dump file
   io.close(ofile)

   -- send a signal to i3status to update immediately
   os.execute("pkill -USR1 i3status")
end

-- Do a dump everytime metadata changes
mp.observe_property("metadata", nil, on_new_metadata)
