#!/bin/sh
# -*- shell-script -*-

# scfzf - Soundcloud Fuzzy Finder
#
# Stupid script to search Soundcloud a'la ytfzf :
# https://github.com/pystardust/ytfzf
#
# WARNING : This uses BSD extensions to grep and sed. It will likely
#           not work on SVR4 and GNU systems.
#
# Copyright 2022 Nico Sonack <nsonack@outlook.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

set -e

# In parts stolen from
# https://github.com/yt-dlp/yt-dlp/blob/master/yt_dlp/extractor/soundcloud.py

_USER_AGENT='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36'

[ "${SCFZF_EXTMENU}" ] || SCFZF_EXTMENU="fzf"
[ "${SCFZF_YTDL}" ]    || SCFZF_YTDL="`which yt-dlp`"                    # Needs to be yt-dlp for now. youtube-dl itself is dead.

extmenu()
{
    ${SCFZF_EXTMENU}
}

get_client_id_urls()
{
    url="https://soundcloud.com/"
    curl --silent -A "${_USER_AGENT}" -4 -L --url ${url} \
        | fgrep '<script' \
        | grep -E '<script[^>]+src="[^"]+"' \
        | sed -E 's/<script[^>]+src="([^"]+)".*/\1/g'
}

CACHEFILE="${HOME}/.cache/scfzf-clientid"

get_client_id()
{
    if [ -f "${CACHEFILE}" ]; then
        cat "${CACHEFILE}"
        return
    fi

    for url in `get_client_id_urls`; do
        curl --silent -A "${_USER_AGENT}" -4 -L --url ${url} \
            | fgrep 'client_id' \
            | awk '{ print(substr($0, match($0, "client_id"), 50)) }' \
            | grep -E '.*client_id[:space:]*:[:space:]*"[0-9a-zA-Z]{32}.*"' \
            | sed -E 's/.*client_id[:space:]*:[:space:]*"([0-9a-zA-Z]{32}).*"/\1/g'
    done | sed 1q | tee "${CACHEFILE}"
}

search()
{
    while ! curl \
              --silent --fail -A "${_USER_AGENT}" -4 -L -G \
              "${_API_V2_BASE}/search" \
              --data-urlencode "q=${*}" \
              --data-urlencode "limit=50" \
              --data-urlencode "offset=0" \
              --data-urlencode "client_id=${_CLIENT_ID}";
    do
        # invalidate cache
        echo "INFO : Cached Soundcloud Client ID did not work. Fetching a new one." 1>&2
        rm "${CACHEFILE}"
        _CLIENT_ID="`get_client_id`"
    done
}

prompt_user()
{
    cat ${TMPFILE} \
        | jq -r '[.collection[] | [.kind, .genre, .publisher_metadata.artist, .title]|@tsv]|join("\n")' \
        | grep -v '^user' \
        | awk -F"\t" '{ printf "%10.10s\t%20.20s\t%20.20s\t%s\n", $1, $2, $3, $4 }' \
        | extmenu
}

get_permalink()
{
    cat ${TMPFILE} \
        | jq -r '[.collection[] | [.kind, .genre, .publisher_metadata.artist, .title, .permalink_url]|@tsv]|join("\n")' \
        | grep -v '^user' \
        | awk -F"\t" '{ printf "%10.10s\t%20.20s\t%20.20s\t%s\t%s\n", $1, $2, $3, $4, $5 }' \
        | grep "^${CHOICE}" \
        | sed -E 's/.*(http.*)$/\1/g' \
        | sed 1q
}

choose_format()
{
    ${SCFZF_YTDL} --compat-options list-formats -F "${LINK}" \
        | awk '/^format/, EOF { print $0 }' \
        | sed 1,1d \
        | extmenu \
        | cut -d\  -f1
}

main()
{
    _CLIENT_ID="`get_client_id`"
    _API_V2_BASE="https://api-v2.soundcloud.com"
    TMPFILE=`mktemp`

    # Perform search
    search $* > ${TMPFILE}

    # Let the user choose
    CHOICE="`prompt_user`"

    # Grab the permalink
    LINK="`get_permalink`"

    if [ ${CHOOSE_FORMAT_MODE} ]; then
        FORMAT="`choose_format`"
        MPV_FORMAT_OPTIONS="--ytdl-format=${FORMAT}"
        YTDL_FORMAT_OPTIONS="-f ${FORMAT}"
    fi

    if [ ${DOWNLOAD_MODE} ]; then
        # TODO: Let the use choose a format
        echo "Downloading: ${LINK}"
        ${SCFZF_YTDL} "${YTDL_FORMAT_OPTIONS}" "${LINK}"
    else
        echo "Playing: ${LINK}"
        mpv --no-video --ytdl --script-opts=ytdl_hook-ytdl_path="${SCFZF_YTDL}" \
            ${MPV_FORMAT_OPTIONS} "${LINK}"
    fi

    rm ${TMPFILE}
}

## ENTRY POINT

args=`getopt df $*`
if [ $? -ne 0 ]; then
    echo 'Usage: $0 [-d] [-f]'
    exit 2
fi
set -- $args
while :; do
    case "$1" in
        -d)
            DOWNLOAD_MODE=1
            shift
            ;;
        -f)
            CHOOSE_FORMAT_MODE=1
            shift
            ;;
        --)
            shift; break
            ;;
    esac
done

# Construct search term
if [ -z "${*}" ]; then
    SEARCH="`extmenu`"
else
    SEARCH="${*}"
fi

main "${SEARCH}"
